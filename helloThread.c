#include<stdio.h>
#include<pthread.h>

void * printHelloWorld(void *a) {
  fprintf(stdout, "Hello Duniya!!\n");
}

void* printMyName(void *argv) {
  fprintf(stdout, "Aman\n");
}

int main() {
  pthread_t pid, pid2;
  pthread_create(&pid, NULL, printHelloWorld, NULL);
  pthread_join(pid, NULL);
  pthread_create(&pid2, NULL, printMyName, NULL);
  pthread_join(pid2, NULL);
  fprintf(stdout, "abcd\n");
  return 0;
}
